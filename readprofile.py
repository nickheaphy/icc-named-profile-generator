'''Read a named ICC profile and dump info to screen'''

# This is pretty quick and dirty, just handles a couple of tags and basically brute forces
# the data out... it is really just used so I can test what I am writing using the
# generate.py code.

# This only reads out the minimum tags for a valid Named Profile

import sys
import NumberClasses
import struct
from fxpmath import Fxp

# ------------------------------------------------
def header_info(iccContent):

    # bytes 0..3 = profile size
        profilesize = struct.unpack_from(">i",iccContent,0)

        # CMM Type Sig - 4..7
        CMMsig = struct.unpack_from(">4s",iccContent,4)

        # Profile Version 8..11
        pversion = struct.unpack_from(">I",iccContent,8)

        # Profile/Device Class signature - 12..15
        classsig = struct.unpack_from(">4s", iccContent,12)

        # Color space of data - 16..19
        cspace = struct.unpack_from(">4s", iccContent,16)

        # Profile Connection Space (PCS) - 20..23
        pcs = struct.unpack_from(">4s", iccContent, 20)

        # Date and time this profile was first created - 24..35
        createddate = struct.unpack_from(">HHHHHH", iccContent, 24)

        #‘acsp’ (61637370h) profile file signature - 36..39
        ascp = struct.unpack_from(">4s", iccContent, 36)

        # Primary Platform signature - 40..43
        pps = struct.unpack_from(">4s", iccContent, 40)

        # Flags 44..47
        profileflags = struct.unpack_from(">i",iccContent,44)

        # Device Manufacturer - 48..51
        deviceman = struct.unpack_from(">4s", iccContent, 48)

        # Device Model - 52..55
        devicemodel = struct.unpack_from(">4s", iccContent, 52)

        # Device attributes - 56..63
        deviceattributes = iccContent[56:64]

        # Rendering Intent - 64..67
        renderingintent = struct.unpack_from(">hh",iccContent,64)

        # Illuminant - 68..79
        illum_x_exp, illum_x_man = struct.unpack_from(">hH", iccContent, 68)
        illum_x = illum_x_exp + illum_x_man / 65536

        illum_y_exp, illum_y_man = struct.unpack_from(">hH", iccContent, 72)
        illum_y = illum_y_exp + illum_y_man / 65536

        illum_z_exp, illum_z_man = struct.unpack_from(">hH", iccContent, 76)
        illum_z = illum_z_exp + illum_z_man / 65536

        # Profile Creator Sig - 80..83
        creatorsig = struct.unpack_from(">4s", iccContent, 80)

        # 44 bytes reserved - 84..127

        print("----- HEADER -----")
        print(f"Profile Size: {profilesize[0]}")
        print(f"Profile File Size: {len(iccContent)}")
        print(f"CMM: {CMMsig[0].decode('utf-8')}")
        print(f"Version: {hex(pversion[0])}")
        print(f"Class: {classsig[0].decode('utf-8')}")
        print(f"Colorspace: '{cspace[0].decode('utf-8')}'")
        print(f"PCS: '{pcs[0].decode('utf-8')}'")
        print(f"Created: {createddate[0]}-{createddate[1]}-{createddate[2]} {createddate[3]}:{createddate[4]}:{createddate[5]}")
        print(f"ASCP: '{ascp[0].decode('utf-8')}'")
        print(f"Platform Sig: '{pps[0].decode('utf-8')}'")
        print(f"Profile Flags: {bin(profileflags[0])[2:].zfill(16)}")
        print(f"Device Manufac: '{deviceman[0].decode('utf-8')}'")
        print(f"Device Model: '{devicemodel[0].decode('utf-8')}'")
        print(f"Device Attrib: {deviceattributes}")
        print(f"Rendering Intent: {renderingintent}")
        print(f"Illuminant: {illum_x:.8f} {illum_y:.8f} {illum_z:.8f}")
        print(f"Creator Sig: '{creatorsig[0].decode('utf-8')}'")

        return pcs[0]


# ------------------------------------------------
def wtpt_tag(iccContent):
    '''This tag specifies the media white point and is used for generating ICC-absolute colorimetric intent'''
    # The XYZType contains an array of three encoded values for the XYZ tristimulus values. The number of
    # sets of values is determined from the size of the tag.
    # 0..3 ‘XYZ ’ (58595A20h) type signature
    # 4..7 reserved, must be set to 0
    # 8..end an array of XYZ numbers XYZNumber

    print("----- WTPT TAG -----")
    sig = struct.unpack_from(">4s", iccContent, 0)
    ignore = struct.unpack_from(">i", iccContent, 4)
    print(f"Sig: '{sig[0].decode('utf-8')}'")
    offset = 8
    while offset < len(iccContent):

        x_b = struct.unpack_from(">I", iccContent, offset)
        y_b = struct.unpack_from(">I", iccContent, offset+4)
        z_b = struct.unpack_from(">I", iccContent, offset+8)

        # Fixed point floating numbers - needed an external library as did my head in
        # trying to work it out.
        # https://www.sciencedirect.com/topics/engineering/fixed-point-number
        x = Fxp(format(x_b[0],'#034b'), signed=True, n_words=32, n_frac=16)
        y = Fxp(format(y_b[0],'#034b'), signed=True, n_words=32, n_frac=16)
        z = Fxp(format(z_b[0],'#034b'), signed=True, n_words=32, n_frac=16)

        offset += 12

        print(f"Whitepoint: {x.astype(float):.8f} {y.astype(float):.8f} {z.astype(float):.8f}")


# ------------------------------------------------
def cprt_tag(iccContent):
    '''This tag contains the 7-bit ASCII text copyright information for the profile.'''

    sig = struct.unpack_from(">4s", iccContent, 0)
    ignore = struct.unpack_from(">i", iccContent, 4)
    copyright = struct.unpack_from(f">{len(iccContent)-8}s", iccContent, 8)

    print("----- CPRT TAG -----")
    print(f"Sig: '{sig[0].decode('ascii')}'")
    print(f"Copyright: '{copyright[0]}'")
    print(f"Copyright: '{copyright[0].decode('ascii')}'")


# ------------------------------------------------
def desc_tag(iccContent):
    '''process a desc tag'''

    """ 0..3 ‘desc’ (64657363h) type signature
        4..7 reserved, must be set to 0
        8..11 ASCII invariant description count, including terminating null (description length) uInt32Number
        12..n-1 ASCII invariant description 7-bit ASCII
        n..n+3 Unicode language code uInt32Number
        n+4..n+7 Unicode localizable description count (description length) uInt32Number
        n+8..m-1 Unicode localizable description
        m..m+1 ScriptCode code uInt16Number
        m+2 Localizable Macintosh description count (description length) uInt8Number
        m+3..m+69 Localizable Macintosh description
    """

    sig = struct.unpack_from(">4s", iccContent, 0)
    ignore = struct.unpack_from(">i", iccContent, 4)
    ascii_count = struct.unpack_from(">I",iccContent,8)
    ascii_desc = struct.unpack_from(f">{ascii_count[0]}s",iccContent,12)
    n = 12 + ascii_count[0]
    unicode_lang_code = struct.unpack_from(">I",iccContent,n)
    unicode_count = struct.unpack_from(">I",iccContent,n+4)
    unicode_desc = struct.unpack_from(f">{unicode_count[0]}s",iccContent,n+8)
    m = n+8+unicode_count[0]
    scriptcode_code = struct.unpack_from(">H",iccContent,m)
    mac_count = struct.unpack_from(">B",iccContent,m+2)
    mac_desc = struct.unpack_from(f">{mac_count[0]}s",iccContent,m+3)

    print("----- DESC TAG -----")
    print(f"Sig: {sig[0].decode('utf-8')}")
    print(f"Ascii Count: {ascii_count[0]}")
    print(f"Ascii Desc: {ascii_desc[0].decode('utf-8')}")
    print(f"Unicode Lang: {unicode_lang_code[0]}")
    print(f"Unicode Count: {unicode_count[0]}")
    print(f"Unicide Desc: {unicode_desc[0]}")
    print(f"ScriptCode Code: {scriptcode_code[0]}")
    print(f"Mac Count: {mac_count[0]}")
    print(f"Mac Desc: {mac_desc[0]}")

# ------------------------------------------------
def ncl2_tag(iccContent,pcs):
    '''process a ncl2 tag'''

    """ 0..3 ‘ncl2’ (6E636C32h) type signature
        4..7 reserved, must be set to 0
        8..11 vendor specific flag (least-significant 16 bits reserved for ICC use)
        12..15 count of named colors uInt32Number
        16..19 number of device coordinates for each named color uInt32Number
        20..51 prefix for each color name (32-byte field including null termination) 7-bit ASCII
        52..83 suffix for each color name (32-byte field including null termination) 7-bit ASCII
        84..115 first color root name (32-byte field including null termination) 7-bit ASCII
        116..121 first named color’s PCS coordinates. The
                encoding is the same as the encodings for the
                PCS color spaces as described in Annex A.
                Only 16-bit L*a*b* and XYZ are allowed. The
                number of coordinates is consistent with the
                header’s PCS.
                uInt16Number[]
        122..y first named color’s device coordinates. For
                each coordinate, 0000h represents the minimum
                value for the device coordinate and
                FFFFh represents the maximum value for the
                device coordinate. The number of coordinates
                is given by the "number of device coordinates"
                field. If the "number of device coordinates" field
                is 0, this field is not given.
                uInt16Number[]
        y+1..z if count > 1 the remaining count-1 colors are described in a manner
            consistent with the first named color.
    """

    sig = struct.unpack_from(">4s", iccContent, 0)
    ignore = struct.unpack_from(">i", iccContent, 4)
    flags = struct.unpack_from(">i", iccContent, 8)
    count = struct.unpack_from(">I", iccContent, 12)
    num_coords = struct.unpack_from(">I", iccContent, 16)
    prefix = struct.unpack_from(">32s", iccContent, 20)
    suffix = struct.unpack_from(">32s", iccContent, 52)

    print("----- NCL2 TAG -----")
    print(f"Sig: {sig[0].decode('utf-8')}")
    print(f"Flags: {bin(flags[0])[2:].zfill(16)}")
    print(f"Count: {count[0]}")
    print(f"Number Coords: {num_coords[0]}")
    print(f"Prefix: {prefix[0].decode('ascii')}")
    print(f"Suffix: {suffix[0].decode('ascii')}")

    

    print("----- NCL2 TAG -----")

    offset = 84
    for i in range(count[0]):
        root_colour_name = struct.unpack_from(">32s", iccContent, offset)
        # 6 bytes of colour info
        if pcs == b'Lab ':
            l,a,b = struct.unpack_from(">HHH", iccContent, offset+32)
            # L 16-bit
            # 0 = 0000h = 0
            # 100.0 = FF00h
            # 100 + (25500/65280) = FFFFh = 100.390625
            l = (l / 65535) * (100 + (25500/65280))
            # a,b 16-bit
            # -128.0 = 0000h
            # 0 = 8000h
            # 127.0 = FF00h
            # 127 + (255/256) = FFFFh
            a = (a / 65535) * (127 + (255/256) + 128) - 128
            b = (b / 65535) * (127 + (255/256) + 128) - 128

        else:
            # Untested
            x,y,z = struct.unpack_from(">HHH", iccContent, offset+32)
            # XYZ 16-bit
            # 0 = 0000h
            # 1.0 = 8000h
            # 1 + (32767/32768) = FFFFh
            x = (x / 65535) * (1.0 + (32767/32768))
            y = (y / 65535) * (1.0 + (32767/32768))
            z = (z / 65535) * (1.0 + (32767/32768))
        
        offset_coords = 0
        device_coords = []
        if num_coords[0] > 0:
            # untested
            for j in range(num_coords[0]):
                device_coords.append(struct.unpack_from(">H", iccContent, 122+offset_coords))
            offset_coords += 2*num_coords[0]
        
        offset += 32 + 6 + offset_coords

        print(f"Name: '{root_colour_name[0].decode('ascii')}'")
        if pcs == b'Lab ':
            print(f"Lab: {l} {a} {b}")
        else:
            print(f"XYZ: {x} {y} {z}")
        if len(device_coords) > 0:
            print(f"Device Cords: {device_coords}")

        
    


# ------------------------------------------------
def main(iccfile):

    with open(iccfile,"rb") as icc:

        # first 128 bytes is the profile header
        iccContent = icc.read()

        # process the header and return the profile connection space
        pcs = header_info(iccContent[0:128])
        
        # Tag Table
        tag_count = struct.unpack_from(">i", iccContent, 128)
        print("----- TAGS -----")
        print(f"Tag Count: {tag_count[0]}")

        tag_table = []
        for i in range(tag_count[0]):
            tag_sig = struct.unpack_from(">4s", iccContent, 128+4+(12*i))
            tag_offset = struct.unpack_from(">I", iccContent, 128+4+(12*i)+4)
            tag_size = struct.unpack_from(">I", iccContent, 128+4+(12*i)+8)

            tag = {
                "sig" : tag_sig[0].decode('utf-8'),
                "offset": tag_offset[0],
                "size": tag_size[0]
            }

            tag_table.append(tag)

            print(f"TagSig: '{tag_table[i]['sig']}'")
            print(f"Offset: {tag_table[i]['offset']}")
            print(f"Size: {tag_table[i]['size']}")
        
        # Tags
        for tag in tag_table:
            if tag['sig'] == 'desc':
                desc_tag(iccContent[tag['offset']:tag['offset']+tag['size']])
            if tag['sig'] == 'ncl2':
                ncl2_tag(iccContent[tag['offset']:tag['offset']+tag['size']],pcs)
                pass
            if tag['sig'] == 'wtpt':
                wtpt_tag(iccContent[tag['offset']:tag['offset']+tag['size']])
            if tag['sig'] == 'cprt':
                cprt_tag(iccContent[tag['offset']:tag['offset']+tag['size']])


# ------------------------------------------------
if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        # use a demo icc file
        main('ICCSample/Solid Coated-V3.icc')