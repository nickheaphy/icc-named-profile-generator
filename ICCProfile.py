import NumberClasses
from datetime import datetime
import csv
import io
import struct
from fxpmath import Fxp

# ----------------------------------------------------------------------
class ICCProfile():

    def __init__(self, profiledescription):
        self.header = ProfileHeader()
        self.desc = TextDescriptionType(profiledescription)
        self.ncl2 = Ncl2Tag()
        self.whitepoint = WtptTag(0.964202880859375, 1.00000000, 0.82489013671875)
        self.copywrite = CopyRightTag("https://bitbucket.org/nickheaphy/icc-named-profile-generator")

    def generate(self):

        # need to build the tag table
        # number of tags
        number_of_tags = 4
        bites = struct.pack(">i", number_of_tags)
        offset = 128 + 4 + (number_of_tags * 12)
        # desc tag
        bites += struct.pack(">4s", "desc".encode('ascii')) #sig
        bites += struct.pack(">I", offset) #offset
        bites += struct.pack(">I", len(self.desc)) # size
        offset += len(self.desc)
        # ncl2 tags
        bites += struct.pack(">4s", "ncl2".encode('ascii')) #sig
        bites += struct.pack(">I", offset) #offset
        bites += struct.pack(">I", len(self.ncl2)) # size
        offset += len(self.ncl2)
        # whitepoint
        bites += struct.pack(">4s", "wtpt".encode('ascii')) #sig
        bites += struct.pack(">I", offset) #offset
        bites += struct.pack(">I", len(self.whitepoint)) # size
        offset += len(self.whitepoint)
        # copywrite
        bites += struct.pack(">4s", "cprt".encode('ascii')) #sig
        bites += struct.pack(">I", offset) #offset
        bites += struct.pack(">I", len(self.copywrite)) # size

        self.header.updatesize(128 + len(bites) + len(self.desc) + len(self.ncl2) + len(self.whitepoint) + len(self.copywrite))
        
        return self.header.bites() + bites + self.desc.bites() + self.ncl2.bites() + self.whitepoint.bites() + self.copywrite.bites()

# ----------------------------------------------------------------------
class ProfileHeader():
    '''128 Bytes of Data'''
    def __init__(self, length=0):
        self.profilesize = NumberClasses.uInt32Number(length)
        self.cmmtypesignature = "Lino"
        self.profileversion = "02000000" # 2.0.0
        self.profileclass = "nmcl" # Named Color profile
        self.colorspace = "CMYK"
        self.pcs = "Lab"
        self.primaryplatform = "SGI " # I used to have a SGI Indy :-)
        self.flags = "00000000" # No flags
        self.manufacturer = "NICK"
        self.attributes = "0000000000000000"
        self.renderingintent = struct.pack(">hh",0,1) # Relative Colormetric
        self.illuminant = NumberClasses.XYZNumber(0.964202880859375, 1.00000000, 0.82489013671875)
    
    def updatesize(self, newsize):
        self.profilesize = NumberClasses.uInt32Number(newsize)

    def bites(self):
        now = datetime.now()
        # 0..3 Profile Size (4 bytes)
        bites = self.profilesize.bites()
        # 4..7 CMM Type Signature (4 bytes)
        bites += struct.pack(">4s", self.cmmtypesignature.encode('ascii'))
        # 8..11 Profile Version Number (4 bytes)
        bites += bytes.fromhex(self.profileversion)
        # 12..15 Profile/Device Class signature (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.profileclass))
        # 16..19 Color space of data (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.colorspace))
        # 20..23 Profile Connection Space (PCS) (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.pcs))
        # 24..35 Date and time this profile was first created (12 bytes)
        bites += NumberClasses.uInt16Number(int(now.strftime("%Y"))).bites()
        bites += NumberClasses.uInt16Number(int(now.strftime("%m"))).bites()
        bites += NumberClasses.uInt16Number(int(now.strftime("%d"))).bites()
        bites += NumberClasses.uInt16Number(int(now.strftime("%H"))).bites()
        bites += NumberClasses.uInt16Number(int(now.strftime("%M"))).bites()
        bites += NumberClasses.uInt16Number(int(now.strftime("%S"))).bites()
        # 36..39 ‘acsp’ (61637370h) profile file signature (4 bytes)
        bites += bytes.fromhex("61637370")
        # 40..43 Primary Platform signature (4 bytes)
        bites += struct.pack(">4s", self.primaryplatform.encode('ascii'))
        # 44..47 Flags to indicate various options for the CMM (4 bytes)
        bites += bytes.fromhex(self.flags)
        # 48..51 Device manufacturer of the device for which this profile is created (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.manufacturer))
        # 52..55 Device model of the device for which this profile is created (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.manufacturer))
        # 56..63 Device attributes unique to the particular device setup (8 bytes)
        bites += bytes.fromhex(self.attributes)
        # 64..67 Rendering Intent (4 bytes)
        bites += self.renderingintent
        # 68..79 The XYZ values of the illuminant of the PCS (12 bytes)
        bites += bytes.fromhex(str(self.illuminant))
        # 80..83 Profile Creator signature (4 bytes)
        bites += bytes.fromhex(self.convert_to_four_byte_hex(self.manufacturer))
        # 84..127 44 bytes reserved for future expansion (44 bytes)
        bites += (0).to_bytes(44, byteorder='big', signed=False)
        return bites

    def convert_to_four_byte_hex(self, code):
        return code.ljust(4).encode('ascii').hex()

    def __len__(self):
        return len(self.bites())

# ----------------------------------------------------------------------
class TextDescriptionType():

    def __init__(self, description):
        self.description = description + '\0' # Null Terminated

    def bites(self):
        bites = 'desc'.encode('ascii')
        bites += (0).to_bytes(4, byteorder='big', signed=False)
        bites += NumberClasses.uInt32Number(len(self.description)).bites()
        bites += self.description.encode('ascii')
        bites += NumberClasses.uInt32Number(0).bites() # Unicode language code
        bites += NumberClasses.uInt32Number(0).bites() # Unicode language length
        bites += NumberClasses.uInt16Number(0).bites() # Scriptcode
        bites += NumberClasses.uInt8Number(0).bites() # Scriptcode count
        bites += (0).to_bytes(67, byteorder='big', signed=False)

        return bites

    def __len__(self):
        return len(self.bites())

# ----------------------------------------------------------------------
class CopyRightTag():

    def __init__(self, copyright):
        self.copyright = copyright + '\0' # Null Terminated

    def bites(self):
        #bites = 'cprt'.encode('ascii') - opps - this should not be here!
        bites = 'text'.encode('ascii')
        #bites += (0).to_bytes(4, byteorder='big', signed=False)
        #bites += struct.pack(">I", 0)
        bites += self.copyright.encode('ascii')

        return bites

    def __len__(self):
        return len(self.bites())


# ----------------------------------------------------------------------
class Ncl2Tag():

    def __init__(self, colour_list=None):
        self.colour_list = colour_list
        self.colours = []
        if self.colour_list is not None:
            self.process_colour_list(self.colour_list)

    def process_colourlist(self, colour_list):
        '''process the colourlist'''
        # the colour list is a csv file encoded as
        # l,a,b,name

        # check if we have been passed an open CSV file
        if not isinstance(colour_list, io.IOBase):
            csvfile = open(colour_list, newline='')
        else:
            csvfile = colour_list

        csvreader = csv.DictReader(csvfile)
        for row in csvreader:

            colour = {
                "name" : row['name'],
                "l": row['l'],
                "a": row['a'],
                "b": row['b']
            }
            self.colours.append(colour)

        csvfile.close()

    def bites(self):

        """ 0..3 ‘ncl2’ (6E636C32h) type signature
        4..7 reserved, must be set to 0
        8..11 vendor specific flag (least-significant 16 bits reserved for ICC use)
        12..15 count of named colors uInt32Number
        16..19 number of device coordinates for each named color uInt32Number
        20..51 prefix for each color name (32-byte field including null termination) 7-bit ASCII
        52..83 suffix for each color name (32-byte field including null termination) 7-bit ASCII
        84..115 first color root name (32-byte field including null termination) 7-bit ASCII
        116..121 first named color’s PCS coordinates. The
                encoding is the same as the encodings for the
                PCS color spaces as described in Annex A.
                Only 16-bit L*a*b* and XYZ are allowed. The
                number of coordinates is consistent with the
                header’s PCS.
                uInt16Number[]
        122..y first named color’s device coordinates. For
                each coordinate, 0000h represents the minimum
                value for the device coordinate and
                FFFFh represents the maximum value for the
                device coordinate. The number of coordinates
                is given by the "number of device coordinates"
                field. If the "number of device coordinates" field
                is 0, this field is not given.
                uInt16Number[]
        y+1..z if count > 1 the remaining count-1 colors are described in a manner
            consistent with the first named color.
        """

        bites = struct.pack(">4s", "ncl2".encode('ascii')) #sig
        bites += struct.pack(">i", 0) #reserved
        bites += struct.pack(">i", 0) #flags
        bites += struct.pack(">I", len(self.colours)) #count of colours
        bites += struct.pack(">I", 0) #device coords
        bites += struct.pack(">32s", (b"\0" * 32)) #prefix
        bites += struct.pack(">32s", (b"\0" * 32)) #sufix
        for colour in self.colours:
            bites += struct.pack(">32s", (colour['name'].ljust(31,'\0')+'\0').encode('ascii'))
            # colours need to be 16bit uInt16Number
            # L* encoding
            # 0 00h 0000h
            # 100.0 FF00h
            # 100 + (25500/65280) FFFFh
            l = int(float(colour['l']) / (100 + (25500/65280)) * 65535)
            # a* and b* encoding
            # -128.0 0000h
            # 0 8000h
            # 127.0 FF00h
            # 127 + (255/256) FFFFh
            
            a = int((float(colour['a']) + 128.0) / (128 + 127 + (255/256)) * 65535)
            b = int((float(colour['b']) + 128.0) / (128 + 127 + (255/256)) * 65535)
            bites += struct.pack(">HHH", l, a, b)

        return bites

    def __len__(self):
        return len(self.bites())

# ----------------------------------------------------------------------
class WtptTag():

    def __init__(self, x,y,z):
        self.x = x
        self.y = y
        self.z = z

    def bites(self):
        
        #bites = struct.pack(">4s", "wtpt".encode('ascii')) #sig

        # 0..3 ‘XYZ ’ (58595A20h) type signature
        # 4..7 reserved, must be set to 0
        # 8..end an array of XYZ numbers XYZNumber
        bites = struct.pack(">4s", "XYZ ".encode('ascii')) #type
        bites += struct.pack(">I", 0)
        bites += NumberClasses.XYZNumber(self.x, self.y, self.z).bites()

        return bites
    
    def __len__(self):
        return len(self.bites())

class TagTable():

    def __init__(self, num_tags):
        self.num_tags = num_tags

    def bites(self):
        return struct.pack(">i", iccContent, 128)

    def __len__(self):
        return len(self.bites())



        


