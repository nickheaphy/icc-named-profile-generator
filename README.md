# ICC Named Profile Generator

Tools to generate a named ICC color profile.

## Usage

Edit the `color_list.csv` to include the named colours you want in your ICC profile (note that these need to be in LAB colourspace)

Run `python3 generate.py profilename.icc` to convert the `color_list.csv` into a named ICC profile.

You can use `python3 readprofile.py profilename.icc` to read the profile back in to make sure that nothing bad happened and everything looks the same coming out as it looked going in.

## Notes

This is really ugly code. The ICC profile specifications are really well documented, but converting all the numbers for use in Python was not that straightforward and I came back to this project a few times and had different ideas as to how to approach it, so the code sometimes uses structs to convert numbers to bytes - othertimes it uses the NumberClasses.py (which has a crazy way of creating hex numbers, then converting these to bytes)

At the moment it is working, but it desperately needs refactoring if it was going to be used for anything.

