
import math



# -----------------------------------------------------------------
class NumberClass():

    def __init__(self, num):
        self.num = num

    def bites(self):
        return bytes.fromhex(str(self))

# -----------------------------------------------------------------
class uInt16Number(NumberClass):
    '''This type represents a generic unsigned 2-byte/16-bit quantity'''
    
    def __str__(self):
         # Create two bytes from the integer
        return self.num.to_bytes(2, byteorder='big', signed=False).hex()

# -----------------------------------------------------------------
class uInt32Number(NumberClass):
    '''This type represents a generic unsigned 4-byte/32-bit quantity'''
    
    def __str__(self):
         # Create four bytes from the integer
        return self.num.to_bytes(4, byteorder='big', signed=False).hex()

# -----------------------------------------------------------------
class uInt64Number(NumberClass):
    '''This type represents a generic unsigned 8-byte/64-bit quantity'''

    def __str__(self):
         # Create eight bytes from the integer
        return self.num.to_bytes(8, byteorder='big', signed=False).hex()

# -----------------------------------------------------------------
class uInt8Number(NumberClass):
    '''This type represents a generic unsigned 1-byte/8-bit quantity.'''

    def __str__(self):
         # Create one byte from the integer
        return self.num.to_bytes(1, byteorder='big', signed=False).hex()

# -----------------------------------------------------------------
class s15Fixed16Number(NumberClass):
    '''This type represents a fixed signed 4-byte/32-bit quantity which has 16 fractional bits.'''
    # https://www.sciencedirect.com/topics/engineering/fixed-point-number
    # S(15,16)

    def __str__(self):
        frac, whole = math.modf(float(self.num))
        mantissa = int(whole).to_bytes(2, byteorder='big', signed=True).hex()
        exponent = int(abs(frac) * 65536).to_bytes(2, byteorder='big', signed=False).hex()
        return  mantissa + exponent

# -----------------------------------------------------------------
class u16Fixed16Number(NumberClass):
    '''This type represents a fixed unsigned 4-byte/32-bit quantity which has 16 fractional bits'''
    # https://www.sciencedirect.com/topics/engineering/fixed-point-number
    # U(16,16)

    def __str__(self):
        frac, whole = math.modf(float(self.num))
        mantissa = int(whole).to_bytes(2, byteorder='big', signed=False).hex()
        exponent = int(frac * 65536).to_bytes(2, byteorder='big', signed=False).hex()
        return  mantissa + exponent


# -----------------------------------------------------------------
class u8Fixed8Number(NumberClass):
    '''This type represents a fixed unsigned 2-byte/16-bit quantity which has 8 fractional bits.'''

    def __str__(self):
        frac, whole = math.modf(float(self.num))
        mantissa = int(whole).to_bytes(1, byteorder='big', signed=False).hex()
        exponent = int(frac * 256).to_bytes(1, byteorder='big', signed=False).hex()
        return  mantissa + exponent

# -----------------------------------------------------------------
class profileVersion(NumberClass):
    '''First 8 bits are the major revision and the next 8 bits are for the minor revision and bug fix revision.'''
    def __init__(self, h_version):
        self.hexversion = h_version
    def __str__(self):
        return self.hexversion

# -----------------------------------------------------------------
class XYZNumber(NumberClass):
    '''This type represents a set of three fixed signed 4-byte/32-bit quantities used to encode CIEXYZ tristimulus values'''
    def __init__(self, x,y,z):
        self.x = s15Fixed16Number(x)
        self.y = s15Fixed16Number(y)
        self.z = s15Fixed16Number(z)
    def __str__(self):
        return str(self.x) + str(self.y) + str(self.z)



# -----------------------------------------------------------------
#        UNIT TESTS (from ICC Specifications document)
# -----------------------------------------------------------------
def test_s15Fixed16Number():
    t1 = s15Fixed16Number(-32768.0)
    t2 = s15Fixed16Number(0)
    t3 = s15Fixed16Number(1.0)
    t4 = s15Fixed16Number(32767 + (65535/65536))

    assert(str(t1) == "80000000")
    assert(str(t2) == "00000000")
    assert(str(t3) == "00010000")
    assert(str(t4) == "7FFFFFFF".lower())

    print(f"Test s15Fixed16Number passed")

# -----------------------------------------------------------------
def test_u16Fixed16Number():
    t1 = u16Fixed16Number(0)
    t2 = u16Fixed16Number(1.0)
    t3 = u16Fixed16Number(65535 + (65535/65536))

    assert(str(t1) == "00000000")
    assert(str(t2) == "00010000")
    assert(str(t3) == "FFFFFFFF".lower())

    print(f"Test u16Fixed16Number passed")

# -----------------------------------------------------------------
def test_u8Fixed8Number():
    t1 = u8Fixed8Number(0)
    t2 = u8Fixed8Number(1.0)
    t3 = u8Fixed8Number(255 + (255/256))

    assert(str(t1) == "0000")
    assert(str(t2) == "0100")
    assert(str(t3) == "FFFF".lower())

    print(f"Test u8Fixed8Number passed")

# -----------------------------------------------------------------
def test_profileVersion():
    t1 = profileVersion("02400000")
    assert(str(t1) == "02400000")
    print(f"Test profileVersion passed")

# -----------------------------------------------------------------
if __name__ == "__main__":
    test_s15Fixed16Number()
    test_u16Fixed16Number()
    test_u8Fixed8Number()
    test_profileVersion()
    print("Everything passed")