import ICCProfile
import sys
import os

if __name__ == "__main__":
    
    if len(sys.argv) != 2:
        outputfile = "namedprofile.icc"
    else:
        outputfile = sys.argv[1]

    profile = ICCProfile.ICCProfile("Test")
    profile.ncl2.process_colourlist('color_list.csv')

    with open(outputfile,'wb') as iccFile:
        iccFile.write(profile.generate())
    